#!/bin/sh


[ -n "${REGISTRY_API_TARGET}" ] && sed -Ei "s|#REGISTRY_API_TARGET#|${REGISTRY_API_TARGET}|g" /opt/bitnami/nginx/conf/nginx.conf
[ -n "${LEGAL_NOTICE_LINK}" ] && sed -Ei "s|#LEGAL_NOTICE_LINK#|${LEGAL_NOTICE_LINK}|g" /opt/bitnami/nginx/conf/nginx.conf
[ -n "${PRIVACY_POLICY_LINK}" ] && sed -Ei "s|#PRIVACY_POLICY_LINK#|${PRIVACY_POLICY_LINK}|g" /opt/bitnami/nginx/conf/nginx.conf
if [ -z "$IS_PULLTHROUGH_PROXY" ]; then
  IS_PULLTHROUGH_PROXY=true
fi
[ -n "${IS_PULLTHROUGH_PROXY}" ] && sed -Ei "s|#IS_PULLTHROUGH_PROXY#|${IS_PULLTHROUGH_PROXY}|g" /opt/bitnami/nginx/conf/nginx.conf



cat /opt/bitnami/nginx/conf/nginx.conf
sleep 2


/opt/bitnami/scripts/nginx/entrypoint.sh /opt/bitnami/scripts/nginx/run.sh

# What is this?
A gui for the registry api



# Required Variables
```shell
REGISTRY_API_TARGET=http://api:3030
LEGAL_NOTICE_LINK=https://example.com/#/legal/notice
PRIVACY_POLICY_LINK=https://example.com/#/legal/data

```



# Cleanup old docker images and voluems
```
docker system prune -af --volumes

```


## Requirements
```
npm uninstall -g @ionic/cli
npm install -g @ionic/cli@latest

ionic start angular blank --type=angular --capacitor

```

## How to integrate PWA components
```
ng add @angular/pwa

npm i --save @ionic/pwa-elements

```



# How to build for architecture arm64?
- https://arborxr.com/blog/developers-journal-building-unprivileged-multi-arch-images-with-kaniko-and-gitlab-ci/



# How to upgrade angular & ionic
```shell
# you can only update one major version at a time!
# ng update @angular/cli @angular/core --force
ANGULAR_MAJOR_VERSION=15
ng update @angular/cli@$ANGULAR_MAJOR_VERSION @angular/core@$ANGULAR_MAJOR_VERSION --force

# manually upgraded:
# - in dependencies:
"@angular/material": "^15.2.9",
# - in dev dependencies:
"@angular-builders/custom-webpack": "^15.0.0",


# https://ionicframework.com/docs/updating/7-0
npm install rxjs@7.5.0
npm install @ionic/angular@7 @ionic/angular-toolkit@9

```

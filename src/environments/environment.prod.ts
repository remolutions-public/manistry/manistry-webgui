export const environment = {
  production: true,
  storageSalt: 'UKIlpMgZwaCXRNAM',
  csrfName: undefined,
  showFooter: true
};

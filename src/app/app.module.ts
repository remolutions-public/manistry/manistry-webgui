import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { registerLocaleData, CommonModule } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { CookieModule } from './@core/services/cookie/cookie.module';
import { BackdropModule } from './@theme/components/backdrop/backdrop.module';
import { CoreModule } from './@core/@core.module';
import { ThemeModule } from './@theme/@theme.module';
import { HeaderModule } from './@theme/components/header/header.module';
import { SidebarModule } from './@theme/components/sidebar/sidebar.module';
import { PagesModule } from './pages/pages.module';
import { FooterModule } from './@theme/components/footer/footer.module';

import { environment } from '../environments/environment';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';

// required for AOT compilation
export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, 'assets/language/', '.json');
};

const COMPONENTS = [AppComponent];

const MODULES = [
  CommonModule,
  BrowserModule,
  BrowserAnimationsModule,
  IonicModule.forRoot(),
  PagesModule,
  ThemeModule,
  CookieModule,
  BackdropModule,
  CoreModule,
  HeaderModule,
  FooterModule,
  SidebarModule,
  HttpClientModule,
  TranslateModule.forRoot({
    defaultLanguage: 'en',
    loader: {
      provide: TranslateLoader,
      useFactory: createTranslateLoader,
      deps: [HttpClient],
    },
  }),
];

const PROVIDERS = [
  { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  providers: [...PROVIDERS],
  bootstrap: [AppComponent],
})
export class AppModule {}

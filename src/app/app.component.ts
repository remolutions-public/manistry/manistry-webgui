import { Component, OnInit } from '@angular/core';
import { SidebarService } from './@core/services/sidebar/sidebar.service';
import { ThemeService } from './@core/services/theme/theme.service';
import { LanguageService } from './@core/services/language/language.service';
import { CookieService } from './@core/services/cookie/cookie.service';
import { BackdropService } from './@core/services/backdrop/backdrop.service';
import { MaterialIconsService } from './@core/services/theme/material-icons/material-icons.service';
import { ContextmenuService } from './@core/services/contextmenu/contextmenu.service';
import { ConfigService } from './@core/services/config/config.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  set menuHidden(val: boolean) {
    this.sidebarService.menuHidden = val;
  }

  get menuHidden(): boolean {
    return this.sidebarService.menuHidden;
  }

  get showFooter(): boolean {
    return environment.showFooter;
  }

  constructor(
    private readonly materialIconsService: MaterialIconsService,
    private readonly sidebarService: SidebarService,
    public readonly backdropService: BackdropService,
    private readonly themeService: ThemeService,
    private readonly languageService: LanguageService,
    public readonly cookieService: CookieService,
    private readonly configService: ConfigService,
    private readonly contextmenuService: ContextmenuService
  ) {}

  ngOnInit() {
    this.materialIconsService.registerIcons();
    this.sidebarService.disableMenuSwiping();
    this.languageService.initLanguage();
    this.themeService.initTheme();
    this.cookieService.onInit();
    this.configService.fetchRecaptchaPublicKey();
  }

  isNativeApp(): boolean {
    return window.origin.includes('capacitor://');
  }

  onScroll(event: any) {
    this.contextmenuService.contextmenuScrollEvent.next(event);
  }
}

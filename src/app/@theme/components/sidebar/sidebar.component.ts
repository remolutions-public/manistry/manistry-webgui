import { Component, OnInit } from '@angular/core';
import { RoutingService } from '../../../@core/services/routing/routing.service';
import { SidebarService } from '../../../@core/services/sidebar/sidebar.service';
import { ThemeService } from '../../../@core/services/theme/theme.service';
import { NotificationService } from '../../../@core/services/toast-notification/toast-notification.service';
import { debounce } from '../../../@core/libraries/basic/debounce.lib';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  isLoading = false;

  isActiveNavigationMap = {
    navigation: true,
    request: true,
    admin: true,
  };

  get headerThemeColor(): string {
    return this.themeService.activeTheme.header;
  }

  constructor(
    private readonly note: NotificationService,
    private readonly routing: RoutingService,
    private readonly sidebarService: SidebarService,
    private readonly themeService: ThemeService
  ) {}

  ngOnInit(): void {
    const mainRoute = window?.location?.hash?.split('/')[1];
    switch (mainRoute) {
      case 'admin':
        this.isActiveNavigationMap.admin = true;
        break;
    }
  }

  isRouteActive(route: string) {
    return this.routing.isRouteActive(route);
  }

  onNavigateTo(path: string[], newWindow?: boolean) {
    if (newWindow) {
      window.open(
        window.location.protocol +
          '//' +
          window.location.host +
          '/' +
          path.filter((element) => element.length && element !== '/').join('/'),
        '_blank'
      );
      return;
    }
    this.routing.navigate(path);
  }

  isActive(key: string) {
    return this.isActiveNavigationMap[key];
  }

  toggleActive(key: string) {
    this.isActiveNavigationMap[key] = !this.isActiveNavigationMap[key];
  }
}

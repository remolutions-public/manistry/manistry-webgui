import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './sidebar.component';

import { MatMenuModule } from '@angular/material/menu';

const COMPONENTS = [
  SidebarComponent,
];

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
];

const ANGULARMATERIAL = [
  MatMenuModule,
];

@NgModule({
    declarations: [...COMPONENTS],
    imports: [...MODULES, ...ANGULARMATERIAL],
    exports: [...COMPONENTS],
    providers: []
})
export class SidebarModule { }

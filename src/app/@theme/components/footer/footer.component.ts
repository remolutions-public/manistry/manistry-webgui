import { Component } from '@angular/core';
import { RoutingService } from '../../../@core/services/routing/routing.service';
import { ConfigService } from '../../../@core/services/config/config.service';
import { debounce } from '../../../@core/libraries/basic/debounce.lib';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  constructor(
    private readonly routing: RoutingService,
    private readonly configService: ConfigService
  ) {}

  async onLegal() {
    const settings = await this.configService.fetchSettings();
    if (settings?.legalNoticeLink == undefined) {
      return;
    }
    window.open(settings?.legalNoticeLink, '_tab');
  }

  async onDataprotection() {
    const settings = await this.configService.fetchSettings();
    if (settings?.legalNoticeLink == undefined) {
      return;
    }
    window.open(settings?.privacyPolicyLink, '_tab');
  }
}

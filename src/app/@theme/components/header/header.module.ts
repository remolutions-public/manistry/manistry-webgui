import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderComponent } from './header.component';
import { LogoModule } from '../logo/logo.module';

import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';

const COMPONENTS = [HeaderComponent];

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
  LogoModule,
];

const ANGULARMATERIAL = [MatMenuModule, MatTooltipModule];

@NgModule({
    declarations: [...COMPONENTS],
    imports: [...MODULES, ...ANGULARMATERIAL],
    exports: [...COMPONENTS],
    providers: []
})
export class HeaderModule {}

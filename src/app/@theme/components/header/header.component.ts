import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../@core/services/auth/auth.service';
import { SidebarService } from '../../../@core/services/sidebar/sidebar.service';
import { LanguageService } from '../../../@core/services/language/language.service';
import { RoutingService } from '../../../@core/services/routing/routing.service';
import {
  Theme,
  ThemeService,
} from '../../../@core/services/theme/theme.service';
import {
  debounce,
  stopDebounceRef,
} from '../../../@core/libraries/basic/debounce.lib';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public userMenuActive = false;
  public activeMenuDropdowns: any = {};

  get username() {
    return this.authService.session.username;
  }

  get menuHidden(): boolean {
    return this.sidebarService.menuHidden;
  }

  get currentTheme(): Theme {
    return this.themeService.currentTheme;
  }

  get currentLanguage(): string {
    return this.languageService.currentLanguage
      ? this.languageService.currentLanguage.toUpperCase()
      : 'DE';
  }

  get availableLanguages(): string[] {
    return this.languageService.supportedLanguages;
  }

  get availableThemes(): Theme[] {
    return this.themeService.availableThemes;
  }

  constructor(
    private readonly routing: RoutingService,
    private readonly authService: AuthService,
    private readonly sidebarService: SidebarService,
    private readonly languageService: LanguageService,
    private readonly themeService: ThemeService
  ) {
    document.body.addEventListener('click', () => {
      this.onMenu();
    });
  }

  ngOnInit(): void {}

  isRouteActive(route: string) {
    return this.routing.isRouteActive(route);
  }

  onMenuClick() {
    this.sidebarService.nextMenuHiddenSub.next(!this.menuHidden);
  }

  isLoggedInSync(): boolean {
    return this.authService.isLoggedInSync();
  }

  onHome() {
    this.routing.navigate(['/']);
  }

  onSignIn() {
    this.onMenu();
    this.routing.navigate(['/', 'auth']);
  }

  onMenu(event?: any, menu?: string) {
    if (!(event == undefined)) {
      event.preventDefault();
      event.stopPropagation();
    }
    for (const entry of Object.keys(this.activeMenuDropdowns)) {
      if (entry !== menu) {
        this.activeMenuDropdowns[entry] = false;
      }
    }
    if (!(menu == undefined) && menu.length) {
      this.activeMenuDropdowns[menu] = !this.activeMenuDropdowns[menu];
    }
  }

  isMenuActive(menu: string): boolean {
    return this.activeMenuDropdowns[menu] || false;
  }

  onLogout() {
    this.authService.logout();
  }

  onSetLanguage(lang: string) {
    this.languageService.switchLanguage(lang);
  }

  onSwitchTheme(theme: any) {
    this.themeService.switchTheme(theme.name);
  }
}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class LogoComponent {
  @Input() width = '50';
  @Input() height = '50';
  @Input() theme = 'default';

  selectedSize: any;
  sources = {
    default: [
      { size: 32, url: 'assets/logos/favicon.png' },
      { size: 64, url: 'assets/logos/favicon.png' },
      { size: 160, url: 'assets/logos/manistry256p.png' },
      { size: 325, url: 'assets/logos/manistry384p.png' },
    ]
    // default: [
    //   { size: 32, url: 'assets/logos/light-logo-32x32.png' },
    //   { size: 64, url: 'assets/logos/light-logo-64x64.png' },
    //   { size: 160, url: 'assets/logos/light-logo-160x160.png' },
    //   { size: 325, url: 'assets/logos/light-logo-325x325.png' },
    // ],
    // white: [
    //   { size: 32, url: 'assets/logos/light-logo-32x32.png' },
    //   { size: 64, url: 'assets/logos/light-logo-64x64.png' },
    //   { size: 160, url: 'assets/logos/light-logo-160x160.png' },
    //   { size: 325, url: 'assets/logos/light-logo-325x325.png' },
    // ],
    // dark: [
    //   // needs to be reworked with e. g. dark background
    //   { size: 32, url: 'assets/logos/dark-logo-32x32.png' },
    //   { size: 64, url: 'assets/logos/dark-logo-64x64.png' },
    //   { size: 160, url: 'assets/logos/dark-logo-160x160.png' },
    //   { size: 325, url: 'assets/logos/dark-logo-325x325.png' },
    // ]
  };

  get imgUrl(): string {
    if (!this.selectedSize) {
      for (const source of this.sources[this.theme]) {
        if (
          source.size > parseInt(this.width, 10) ||
          source.size > parseInt(this.height, 10)
        ) {
          this.selectedSize = source;
          break;
        }
      }
    }
    return this.selectedSize.url;
  }
}

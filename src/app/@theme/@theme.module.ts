import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from './components/header/header.module';
import { LogoModule } from './components/logo/logo.module';
import { SidebarModule } from './components/sidebar/sidebar.module';
import { FooterModule } from './components/footer/footer.module';

export const MODULES = [
  CommonModule,
  HeaderModule,
  LogoModule,
  SidebarModule,
  FooterModule,
];

@NgModule({
  imports: [...MODULES],
  declarations: [],
  exports: [],
})
export class ThemeModule {}

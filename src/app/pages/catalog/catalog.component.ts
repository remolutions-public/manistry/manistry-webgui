import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { RegistryService } from '../../@core/services/registry/registry.service';
import { RoutingService } from '../../@core/services/routing/routing.service';
import { ActivatedRoute } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { debounce } from '../../@core/libraries/basic/debounce.lib';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
})
export class CatalogComponent implements OnInit, AfterViewInit {
  isLoading = false;
  limit = 10;
  offset = 0;
  rowCount = 0;
  filter = '';
  orderBy: string[];

  catalog: MatTableDataSource<any[]> = new MatTableDataSource([]);
  displayedColumns: string[] = ['actions', 'name'];
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private readonly registryService: RegistryService,
    private readonly routing: RoutingService,
    private readonly actRoute: ActivatedRoute
  ) {}

  async ngOnInit() {
    await this.fetchCatalog();
  }

  ngAfterViewInit() {}

  async fetchCatalog() {
    this.isLoading = true;

    const result = await this.registryService
      .fetchCatalog(this.limit, this.offset, this.filter, this.orderBy)
      .catch((err) => console.error(err));

    this.rowCount = result?.rows || 0;
    this.catalog = new MatTableDataSource(result?.catalog || []);
    this.isLoading = false;
  }

  onRefresh() {
    this.isLoading = true;
    debounce('CatalogComponent-onRefresh', 250, async () => {
      await this.fetchCatalog();
    });
  }

  onLoadingStateChange(state: any) {
    this.isLoading = state;
    if (!this.isLoading) {
      this.fetchCatalog();
    }
  }

  onFilterChange(event: any) {
    this.isLoading = true;
    const val = event.target.value;
    debounce('onFilterChange', 500, () => {
      this.filter = val;
      this.fetchCatalog();
      this.isLoading = false;
    });
  }

  onClearFilter() {
    this.onFilterChange({ target: { value: '' } });
  }

  async onSortChange() {
    this.isLoading = true;
    const column = this.sort.active;
    const direction = this.sort.direction;
    if (direction == undefined || !direction.length) {
      this.orderBy = undefined;
    } else {
      this.orderBy = [column, direction.toUpperCase()];
    }
    debounce('onSortChange', 250, async () => {
      await this.fetchCatalog();
    });
  }

  async onPagination(event: any) {
    this.isLoading = true;
    this.limit = event.pageSize;
    this.offset = event.pageIndex;
    debounce('onPagination', 250, async () => {
      await this.fetchCatalog();
    });
  }

  onTags(image: any) {
    this.routing.navigate(['/', 'tags'], { queryParams: { name: image.name } });
  }
}

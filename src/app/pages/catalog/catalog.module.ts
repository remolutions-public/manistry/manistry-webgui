import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { CatalogRoutingModule } from './catalog-routing.module';

import { CatalogComponent } from './catalog.component';

import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import {
  MatPaginatorModule,
  MatPaginatorIntl,
} from '@angular/material/paginator';
import { PaginatorLanguage } from '../../@core/services/language/paginator/paginator.language';

const ANGULARMATERIAL = [
  MatTooltipModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
];

const COMPONENTS = [
  CatalogComponent,
];

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
  CatalogRoutingModule,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, ...ANGULARMATERIAL],
  providers: [
    {
      provide: MatPaginatorIntl,
      deps: [TranslateService],
      useFactory: (translateService: TranslateService) =>
        new PaginatorLanguage(translateService),
    },
  ],
  exports: [...COMPONENTS],
})
export class CatalogModule {}

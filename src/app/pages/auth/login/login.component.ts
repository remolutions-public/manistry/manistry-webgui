import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../@core/services/auth/auth.service';
import {
  MyErrorStateMatcher,
  ErrorKeys,
} from '../../../@core/libraries/error/error-state-matcher';
import { validationMessages } from '../../../@core/libraries/error/validation-error-messages';
import { RoutingService } from '../../../@core/services/routing/routing.service';
import { RecaptchaService } from '../../../@core/services/recaptcha/recaptcha.service';
import { ConfigService } from '../../../@core/services/config/config.service';
import { NotificationService } from '../../../@core/services/toast-notification/toast-notification.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLoading = false;
  hidePw = true;
  readonly errorMessages = validationMessages;
  readonly matcher = new MyErrorStateMatcher();
  readonly firstError = ErrorKeys.firstError;
  readonly errMsgKeys = ErrorKeys.errMsgKeys;
  loginForm: FormGroup;

  private maxInputChars = 100;
  private minInputChars = 3;

  get recaptchaType() {
    return this.recaptchaService.recaptchaType;
  }

  get recaptchaLanguage() {
    return this.recaptchaService.recaptchaLanguage;
  }

  get recaptchaTheme() {
    return this.recaptchaService.recaptchaTheme;
  }

  get recaptchaSize() {
    return this.recaptchaService.recaptchaSize;
  }

  get recaptchaSiteKey(): string {
    if (this.configService.recaptchaSiteKey == undefined) {
      this.configService.fetchRecaptchaPublicKey();
    }
    return this.configService.recaptchaSiteKey;
  }

  constructor(
    private readonly authService: AuthService,
    private readonly note: NotificationService,
    private readonly routing: RoutingService,
    private readonly recaptchaService: RecaptchaService,
    private readonly configService: ConfigService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  ionViewDidEnter() {
    this.initForm();
  }

  initForm() {
    this.isLoading = true;
    this.loginForm = new FormGroup(
      {
        username: new FormControl('', [
          Validators.required,
          Validators.minLength(this.minInputChars),
          Validators.maxLength(this.maxInputChars),
        ]),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(this.minInputChars),
          Validators.maxLength(this.maxInputChars),
        ]),
        recaptcha: new FormControl('', [Validators.required]),
      },
      []
    );

    setTimeout(() => {
      this.isLoading = false;
    }, 250);
  }

  async onLogin() {
    this.isLoading = true;
    await this.authService
      .login({
        username: this.loginForm.get('username').value,
        password: this.loginForm.get('password').value,
        recaptcha: this.loginForm.get('recaptcha').value,
      })
      .then((authRes) => {
        if (authRes) {
          const currentPath = this.routing.getCurrPath();
          if (
            !(currentPath == undefined) &&
            currentPath.length &&
            currentPath.indexOf('auth') === -1
          ) {
            this.routing.navigate([currentPath]);
          } else {
            this.routing.navigate(['/', 'catalog']);
          }
        } else {
          throw new Error('Did not receive auth object');
        }
      })
      .catch((err) => {
        if (!environment.production) {
          console.error(err);
        }
        this.note.overrideTimeoutInMs(15000);
        this.note.showNotification(
          'error',
          undefined,
          'AUTH.LOGIN.NOTIFICATION.AUTHERROR'
        );
        this.note.resetTimeoutDelayed();
      });
    this.isLoading = false;
  }
}

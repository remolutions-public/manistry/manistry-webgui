import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../../@core/pipes/pipes.module';
import { NgxCaptchaModule } from 'ngx-captcha';

import { AuthRoutingModule } from './auth-routing.module';
import { PasswordStrengthModule } from '../../@core/libraries/validators/components/password-strength.module';

import { LoginComponent } from './login/login.component';

import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';

const ANGULAR_MATERIAL = [
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
];

const COMPONENTS = [
  LoginComponent,
];

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  IonicModule,
  TranslateModule,
  AuthRoutingModule,
  PipesModule,
  NgxCaptchaModule,
  PasswordStrengthModule,
];

@NgModule({
    declarations: [...COMPONENTS],
    imports: [...MODULES, ...ANGULAR_MATERIAL],
    providers: [],
    exports: [...COMPONENTS]
})
export class AuthModule { }

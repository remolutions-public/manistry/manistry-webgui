import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../@core/guards/auth.guard';

export const routes: Routes = [
  {
    path: 'catalog',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./catalog/catalog.module').then((m) => m.CatalogModule),
  },
  {
    path: 'tags',
    canActivate: [AuthGuard],
    loadChildren: () => import('./tag/tag.module').then((m) => m.TagModule),
  },
  {
    path: 'manifests',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./manifest/manifest.module').then((m) => m.ManifestModule),
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: '',
    redirectTo: 'catalog',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      useHash: true,
    }),
  ],
  exports: [RouterModule],
})
export class PagesRoutingModule {}

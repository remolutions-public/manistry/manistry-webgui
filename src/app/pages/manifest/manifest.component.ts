import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { RegistryService } from '../../@core/services/registry/registry.service';
import { NotificationService } from '../../@core/services/toast-notification/toast-notification.service';
import { RoutingService } from '../../@core/services/routing/routing.service';
import { ActivatedRoute } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { debounce } from '../../@core/libraries/basic/debounce.lib';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-manifest',
  templateUrl: './manifest.component.html',
  styleUrls: ['./manifest.component.scss'],
})
export class ManifestComponent implements OnInit, AfterViewInit {
  isLoading = false;

  repoName: string;
  tag: string;

  manifest: any;

  constructor(
    private readonly registryService: RegistryService,
    private readonly routing: RoutingService,
    private readonly actRoute: ActivatedRoute,
    private readonly note: NotificationService,
    private readonly translate: TranslateService,
    public readonly alertController: AlertController
  ) {}

  async ngOnInit() {
    this.repoName = this.actRoute.snapshot.queryParams.name;
    this.tag = this.actRoute.snapshot.queryParams.tag;
    await this.fetchManifest();
  }

  ngAfterViewInit() {}

  async fetchManifest() {
    this.isLoading = true;

    const result = await this.registryService
      .fetchManifest(this.repoName, this.tag)
      .catch((err) => console.error(err));

    this.manifest = result;
    this.isLoading = false;
  }

  onRefresh() {
    this.isLoading = true;
    debounce('ManifestComponent-onRefresh', 250, async () => {
      await this.fetchManifest();
    });
  }

  onLoadingStateChange(state: any) {
    this.isLoading = state;
    if (!this.isLoading) {
      this.fetchManifest();
    }
  }

  onBack(force?: boolean) {
    this.routing.navigate(['/', 'tags'], {
      queryParams: { name: this.repoName, force },
    });
  }

  async onDelete() {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    const confirmation = await new Promise(async (resolve) => {
      const modal = await this.alertController.create({
        cssClass: 'delete-confirmation',
        header: this.translate.instant(
          'MANIFESTS.MODAL.DELETE_CONFIRMATION_TITLE'
        ),
        message: this.translate.instant(
          'MANIFESTS.MODAL.DELETE_CONFIRMATION_MESSAGE'
        ),
        buttons: [
          {
            text: this.translate.instant(
              'MANIFESTS.MODAL.DELETE_CONFIRMATION_CANCEL_BTN'
            ),
            role: 'cancel',
            cssClass: 'delete-confirmation-cancel-btn',
            handler: () => {
              resolve(false);
            },
          },
          {
            text: this.translate.instant(
              'MANIFESTS.MODAL.DELETE_CONFIRMATION_CONFIRM_BTN'
            ),
            cssClass: 'delete-confirmation-confirm-btn',
            handler: () => {
              resolve(true);
            },
          },
        ],
      });
      await modal.present();
    });
    if (confirmation) {
      const result = await this.registryService
        .deleteTag(this.repoName, this.tag)
        .catch((err) => {
          if (!environment.production) {
            console.error(err);
          }
        });
      if (result == undefined) {
        this.note.showNotification(
          'error',
          undefined,
          'MANIFESTS.NOTIFICATION.DELETE_FAILED'
        );
      } else {
        this.note.showNotification(
          'success',
          undefined,
          'MANIFESTS.NOTIFICATION.DELETE_SUCCESS'
        );
      }

      this.onBack(true);
    }

    this.isLoading = false;
  }

  formatSize(size: number) {
    if (size == undefined) {
      return 0;
    }

    const GB = 1024 * 1024 * 1024;
    const MB = 1024 * 1024;
    const KB = 1024;

    const gbsize = size / GB;
    if (Math.round(gbsize) > 0) {
      return Math.round(gbsize * 100) / 100 + ' GB';
    }

    const mbsize = size / MB;
    if (Math.round(mbsize) > 0) {
      return Math.round(mbsize * 100) / 100 + ' MB';
    }

    const kbsize = size / KB;
    if (Math.round(kbsize) > 0) {
      return Math.round(kbsize * 100) / 100 + ' KB';
    }

    return size + ' Bytes';
  }
}

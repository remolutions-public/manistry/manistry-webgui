import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ManifestComponent } from './manifest.component';

export const routes: Routes = [
  {
    path: '',
    component: ManifestComponent,
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManifestRoutingModule {}

import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { RegistryService } from '../../@core/services/registry/registry.service';
import { NotificationService } from '../../@core/services/toast-notification/toast-notification.service';
import { RoutingService } from '../../@core/services/routing/routing.service';
import { ConfigService } from '../../@core/services/config/config.service';
import { ActivatedRoute } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { debounce } from '../../@core/libraries/basic/debounce.lib';
import { environment } from '../../../environments/environment';
import { parseBoolean } from 'src/app/@core/libraries/basic/parser.lib';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss'],
})
export class TagComponent implements OnInit, AfterViewInit {
  isLoading = false;
  limit = 10;
  offset = 0;
  rowCount = 0;
  filter = '';
  orderBy: string[];

  onlyCachedTags = true;
  isPullthroughProxy = false;

  repoName: string;
  forceRefresh = false;

  tags: MatTableDataSource<any[]> = new MatTableDataSource([]);
  displayedColumns: string[] = ['actions', 'timestamp', 'tag'];
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private readonly registryService: RegistryService,
    private readonly routing: RoutingService,
    private readonly actRoute: ActivatedRoute,
    private readonly note: NotificationService,
    private readonly translate: TranslateService,
    private readonly configService: ConfigService,
    public readonly alertController: AlertController
  ) {}

  async ngOnInit() {
    this.repoName = this.actRoute.snapshot.queryParams.name;
    this.forceRefresh = this.actRoute.snapshot.queryParams.force;
    await this.fetchTags();
  }

  ngAfterViewInit() {}

  async fetchTags(force?: boolean) {
    this.isLoading = true;

    const settings = await this.configService.fetchSettings();
    if (!(settings?.isPullthroughProxy == undefined)) {
      this.isPullthroughProxy = parseBoolean(settings?.isPullthroughProxy);
    }

    const result = await this.registryService
      .fetchTags(
        this.repoName,
        this.limit,
        this.offset,
        this.filter,
        this.orderBy,
        this.isPullthroughProxy && this.onlyCachedTags,
        force || this.forceRefresh
      )
      .catch((err) => console.error(err));
    this.rowCount = result?.rows || 0;
    this.tags = new MatTableDataSource(result?.tags || []);
    this.isLoading = false;
  }

  onOnlyCached() {
    this.onlyCachedTags = !this.onlyCachedTags;
    this.onRefresh();
  }

  onRefresh() {
    this.isLoading = true;
    debounce('TagsComponent-onRefresh', 250, async () => {
      await this.fetchTags();
    });
  }

  onForceRefresh() {
    this.isLoading = true;
    debounce('TagsComponent-onForceRefresh', 250, async () => {
      await this.fetchTags(true);
    });
  }

  onLoadingStateChange(state: any) {
    this.isLoading = state;
    if (!this.isLoading) {
      this.fetchTags();
    }
  }

  onFilterChange(event: any) {
    this.isLoading = true;
    const val = event.target.value;
    debounce('onFilterChange', 500, () => {
      this.filter = val;
      this.fetchTags();
      this.isLoading = false;
    });
  }

  onClearFilter() {
    this.onFilterChange({ target: { value: '' } });
  }

  async onSortChange() {
    this.isLoading = true;
    const column = this.sort.active;
    const direction = this.sort.direction;
    if (direction == undefined || !direction.length) {
      this.orderBy = undefined;
    } else {
      this.orderBy = [column, direction.toUpperCase()];
    }
    debounce('onSortChange', 250, async () => {
      await this.fetchTags();
    });
  }

  async onPagination(event: any) {
    this.isLoading = true;
    this.limit = event.pageSize;
    this.offset = event.pageIndex;
    debounce('onPagination', 250, async () => {
      await this.fetchTags();
    });
  }

  onManifest(row: any) {
    this.routing.navigate(['/', 'manifests'], {
      queryParams: { name: this.repoName, tag: row.tag },
    });
  }

  async onDelete(row: any) {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;

    const confirmation = await new Promise(async (resolve) => {
      const modal = await this.alertController.create({
        cssClass: 'delete-confirmation',
        header: this.translate.instant('TAGS.MODAL.DELETE_CONFIRMATION_TITLE'),
        message: this.translate.instant(
          'TAGS.MODAL.DELETE_CONFIRMATION_MESSAGE'
        ),
        buttons: [
          {
            text: this.translate.instant(
              'TAGS.MODAL.DELETE_CONFIRMATION_CANCEL_BTN'
            ),
            role: 'cancel',
            cssClass: 'delete-confirmation-cancel-btn',
            handler: () => {
              resolve(false);
            },
          },
          {
            text: this.translate.instant(
              'TAGS.MODAL.DELETE_CONFIRMATION_CONFIRM_BTN'
            ),
            cssClass: 'delete-confirmation-confirm-btn',
            handler: () => {
              resolve(true);
            },
          },
        ],
      });
      await modal.present();
    });
    if (confirmation) {
      const result = await this.registryService
        .deleteTag(this.repoName, row.tag)
        .catch((err) => {
          if (!environment.production) {
            console.error(err);
          }
        });
      if (result == undefined) {
        this.note.showNotification(
          'error',
          undefined,
          'TAGS.NOTIFICATION.DELETE_FAILED'
        );
      } else {
        this.note.showNotification(
          'success',
          undefined,
          'TAGS.NOTIFICATION.DELETE_SUCCESS'
        );
      }

      debounce('refetch-tags', 500, async () => {
        await this.fetchTags(true);
        this.isLoading = false;
      });
    } else {
      this.isLoading = false;
    }
  }

  onBack() {
    this.routing.navigate(['/', 'catalog']);
  }
}

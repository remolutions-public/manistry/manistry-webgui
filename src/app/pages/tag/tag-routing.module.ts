import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { TagComponent } from './tag.component';

export const routes: Routes = [
  {
    path: '',
    component: TagComponent,
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TagRoutingModule {}

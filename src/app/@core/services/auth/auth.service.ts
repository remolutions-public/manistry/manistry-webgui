import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Session } from '../../libraries/models/session.model';
import { StorageService } from '../storage/storage.service';
import { Subject, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../../environments/environment';
import { RoutingService } from '../routing/routing.service';
import { CookieService } from '../cookie/cookie.service';
import { ConfigService } from '../config/config.service';
import { debounce } from '../../libraries/basic/debounce.lib';
import { sleep } from '../../libraries/basic/sleep.lib';
import { firstValueFrom } from 'rxjs';
import {
  hasCsrfToken,
  getCsrfToken,
  setCsrfToken,
} from '../../libraries/basic/csrf.lib';

@Injectable()
export class AuthService {
  public session: Session = new Session();

  // triggered by auth.service;
  public readonly setSessionSub: Subject<Session> = new Subject();

  initDelayDone = false;
  isLoggingIn = false;

  private minRestTokenDuration = 6 * 24 * 60 * 60 * 1000;

  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly translateService: TranslateService,
    private readonly routing: RoutingService,
    private readonly cookieService: CookieService,
    private readonly configService: ConfigService
  ) {
    this.setSessionSub.subscribe((session) => {
      this.session = session;
      this.checkActiveSession();
    });

    this.refresh();
    this.checkActiveSession();
  }

  async checkActiveSession(customDelay?: number): Promise<void> {
    // check session validity
    const csrfToken = await this.getCsrfToken();
    if (!(csrfToken == undefined)) {
      if (!this.isLoggingIn) {
        if (await this.isLoggedIn()) {
          if (
            !(this.session == undefined) &&
            !(this.session.username == undefined)
          ) {
            const decoded = JSON.parse(atob(csrfToken.split('.')[1]));
            if (
              decoded == undefined ||
              decoded.username !== this.session.username
            ) {
              this.clearSession();
            }
          }
        } else {
          await this.refresh();
        }
      }
    } else {
      if (!this.isLoggingIn) {
        if (await this.isLoggedIn()) {
          this.clearSession();
        }
      }
    }

    // if not logged in simply restart cycle
    const increasedCustomDelay = 2500;
    if (!(await this.isLoggedIn()) && customDelay < increasedCustomDelay) {
      // reduce cycle speed
      this.checkActiveSession(increasedCustomDelay);
      return;
    }

    // try auto refresh before session expiration
    if (
      this.session.tokenExpiration - new Date().getTime() <
      this.minRestTokenDuration
    ) {
      await this.refresh();
    }
    debounce('auth-checkActiveSession', customDelay || 500, async () => {
      this.checkActiveSession();
    });
  }

  async isLoggedIn(): Promise<boolean> {
    // HINT: did you call this function with an await???
    if (!this.initDelayDone) {
      await sleep(250);
      this.initDelayDone = true;
    }
    return this.isLoggedInSync();
  }

  isLoggedInSync(): boolean {
    if (
      !(this.session == undefined) &&
      Object.keys(this.session).length > 0 &&
      !(this.session.username == undefined)
    ) {
      return true;
    }
    return false;
  }

  async login(authObj: {
    username: string;
    password: string;
    recaptcha?: string;
    otp?: string;
  }): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.isLoggedInSync()) {
        await this.logout();
      }
      const auth = await this.makeAuthHeader();
      // + '?' + environment.csrfName + '=' + auth?.token
      firstValueFrom(
        this.http.post('/api/v1/auth', authObj, {
          headers: auth?.headers,
        })
      )
        .then(async (result: any) => {
          resolve(await this.setSessionFromUserData(result));
        })
        .catch((err) => reject(err));
    });
  }

  private async setSessionFromUserData(userData: any): Promise<any> {
    const isCsrfValid = await setCsrfToken(
      userData.csrfToken,
      userData.username
    );
    if (isCsrfValid) {
      this.setIsLogginIn();
      this.setSessionSub.next(userData);
      return userData;
    } else {
      const emptySession = new Session();
      this.setSessionSub.next(emptySession);
      return emptySession;
    }
  }

  async refresh(): Promise<void> {
    return new Promise(async (resolve) => {
      if (
        !(await hasCsrfToken(
          this.session ? this.session.username : undefined,
          this.storageService
        ))
      ) {
        return resolve();
      }

      const auth = await this.makeAuthHeader();
      if (!(await this.isAuthHeaderValid(auth?.headers))) {
        return resolve();
      }

      // + '?' + environment.csrfName + '=' + auth?.token
      firstValueFrom(
        this.http.get('/api/v1/auth', {
          headers: auth.headers,
          withCredentials: true,
        })
      )
        .then((result: any) => {
          if (result.csrfToken) {
            const isCsrfValid = setCsrfToken(result.csrfToken, result.username);
            if (isCsrfValid) {
              this.setSessionSub.next(result);
            } else {
              const emptySession = new Session();
              this.setSessionSub.next(emptySession);
            }
          } else if (result.message === 'error') {
            if (!environment.production) {
              console.error(result.data);
            }
          }
          resolve();
        })
        .catch((err) => {
          if (!environment.production) {
            console.warn(
              'auth refresh failed, this message is only shown in dev mode.',
              err
            );
          }
          // force logout of all auth tokens, triggers a delete request from backend for all sent http secure cookies
          this.logout();
          resolve();
        });
    });
  }

  async logout(skipRedirect?: boolean): Promise<void> {
    return new Promise(async (resolve, reject) => {
      const auth = await this.makeAuthHeader();
      // + '?' + environment.csrfName + '=' + auth?.token
      await firstValueFrom(
        this.http.delete('/api/v1/auth/' + (this.session.username || 'all'), {
          headers: auth?.headers,
        })
      ).catch((err) => reject(err));
      await setCsrfToken(undefined, this.session.username);
      this.clearSession(skipRedirect);
      resolve();
    });
  }

  clearSession(skipRedirect?: boolean) {
    this.setSessionSub.next(new Session());
    if (!skipRedirect) {
      this.routing.navigate(['/', 'auth']);
    }
  }

  async makeAuthHeader(): Promise<{ headers: HttpHeaders; token: string }> {
    return new Promise((resolve) => {
      setTimeout(async () => {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json');
        const token = await this.getCsrfToken();
        if (!(token == undefined) && token.length) {
          if (environment.csrfName == undefined) {
            await this.configService.fetchTokenConfig().catch((err) => {
              if (!environment.production) {
                console.error(err);
              }
            });
          }
          headers = headers.set(environment.csrfName, token);
        }
        resolve({ headers, token });
      }, 0);
    });
  }

  async getCsrfToken() {
    if (environment.csrfName == undefined) {
      await this.configService.fetchTokenConfig().catch((err) => {
        if (!environment.production) {
          console.error(err);
        }
      });
    }
    return await getCsrfToken(
      this.session ? this.session.username : undefined,
      this.storageService
    );
  }

  getIdentity() {
    return this.session?.email || 'anonymous';
  }

  async isAuthHeaderValid(header: HttpHeaders): Promise<boolean> {
    if (environment.csrfName == undefined) {
      await this.configService.fetchTokenConfig().catch((err) => {
        if (!environment.production) {
          console.error(err);
        }
      });
    }
    const token = header.get(environment.csrfName);
    if (token == undefined || !token.length) {
      return false;
    }

    const decoded = JSON.parse(atob(token.split('.')[1]));
    if (!(decoded == undefined) && !(decoded.exp == undefined)) {
      const exp = parseFloat(decoded.exp) * 1000;
      if (exp <= new Date().valueOf()) {
        return false;
      }
    }

    return true;
  }

  setIsLogginIn() {
    // keep it from immediately refreshing the session while it's still in login phase
    this.isLoggingIn = true;
    debounce('setIsLogginIn', 2500, () => {
      this.isLoggingIn = false;
    });
  }

  isAdmin(): boolean {
    // if (!(environment['isAdmin'] == undefined)) {
    //   this.session.isAdmin = parseBoolean(environment['isAdmin']);
    // } else if (this.session.isAdmin == undefined) {
    //   if (this.isRequestingAdminGUI === false) {
    //     this.isRequestingAdminGUI = true;
    //     this.http
    //       .get('/admin', {
    //         observe: 'response',
    //       })
    //       .toPromise()
    //       .then((result: any) => {
    //         const email = result?.headers.get('gap-auth');
    //         this.session.email = email;
    //         this.session.isAdmin = parseBoolean(result?.body?.isAdmin);
    //         this.isRequestingAdminGUI = false;
    //       })
    //       .catch((err) => {
    //         if (!environment.production) {
    //           console.error(err);
    //         }
    //         this.session.isAdmin = false;
    //         this.isRequestingAdminGUI = false;
    //       });
    //   }
    // }
    // return !!this.session?.isAdmin;
    return false;
  }
}

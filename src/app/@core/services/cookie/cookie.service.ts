import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { StorageService } from '../storage/storage.service';

@Injectable()
export class CookieService {
  private cookieName = 'manistry_cookies_accepted';
  public showCookieWarning = true;
  public isFadeOut = false;

  // is triggered by cookie.service; listened by user.service
  public cookieSettingsCookieWasSetSub = new Subject();

  constructor(private readonly storage: StorageService) {}

  onInit() {
    this.checkCookieWarning();
  }

  checkCookieWarning() {
    if (!this.hasCookieAllowance()) {
      this.showCookieWarning = true;
    } else {
      this.showCookieWarning = false;
    }
  }

  hasCookieAllowance(): boolean {
    const cookiesAccepted = this.storage.get(this.cookieName, 'local');
    if (!(cookiesAccepted == undefined)) {
      return true;
    }
    return false;
  }

  acceptCookies(settings: {
    essential: boolean;
    analytics: boolean;
    marketing: boolean;
    date?: string;
  }) {
    settings.date = new Date().toISOString();
    this.storage.set(this.cookieName, settings, 'local');
    // wait for storage to be written...
    setTimeout(() => {
      if (this.hasCookieAllowance()) {
        this.isFadeOut = true;
        setTimeout(() => {
          this.cookieSettingsCookieWasSetSub.next(true);
          this.showCookieWarning = false;
        }, 300);
      }
    }, 10);
  }

  get essential(): boolean {
    const e = this.storage.get(this.cookieName, 'local');
    return e ? e['essential'] : false;
  }

  get analytics(): boolean {
    const e = this.storage.get(this.cookieName, 'local');
    return e ? e['analytics'] : false;
  }

  get marketing(): boolean {
    const e = this.storage.get(this.cookieName, 'local');
    return e ? e['marketing'] : false;
  }

  get cookiedate(): Date {
    const e = this.storage.get(this.cookieName, 'local');
    return e ? e['date'] : new Date().toISOString();
  }

  get cookiesettings() {
    return {
      essential: this.essential,
      analytics: this.analytics,
      marketing: this.marketing,
      date: this.cookiedate
    };
  }
}

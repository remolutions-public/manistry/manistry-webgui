import { Component, OnInit } from '@angular/core';
import { CookieService } from './cookie.service';
import { BackdropService } from '../backdrop/backdrop.service';
import { ConfigService } from '../config/config.service';

@Component({
  selector: 'app-cookie',
  templateUrl: './cookie.component.html',
  styleUrls: ['./cookie.component.scss'],
})
export class CookieComponent implements OnInit {
  isSettingsActive = false;
  essential = true;
  analytics = true;
  marketing = true;

  constructor(
    private readonly cookieService: CookieService,
    private readonly configService: ConfigService,
    public readonly backdropService: BackdropService
  ) {}

  ngOnInit() {}

  async onDataProtectionDeclarationClick() {
    const settings = await this.configService.fetchSettings();
    if (settings?.privacyPolicyLink == undefined) {
      return;
    }
    window.open(settings?.privacyPolicyLink, '_tab');
  }

  onCookieAcceptClick() {
    this.cookieService.acceptCookies({
      essential: this.essential,
      analytics: this.analytics,
      marketing: this.marketing,
    });
  }

  onSettingsClick() {
    this.isSettingsActive = true;
    this.analytics = false;
    this.marketing = false;
  }

  onEssential(event: any) {
    this.essential = event.detail.checked;
  }

  onAnalytics(event: any) {
    this.analytics = event.detail.checked;
  }

  onMarketing(event: any) {
    this.marketing = event.detail.checked;
  }
}

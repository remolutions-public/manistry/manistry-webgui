import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { CookieComponent } from './cookie.component';
import { CookieService } from './cookie.service';

const MODULES = [
  CommonModule,
  TranslateModule,
  IonicModule,
];

const COMPONENTS = [
  CookieComponent,
];

const SERVICES = [
  CookieService,
];

@NgModule({
    declarations: [...COMPONENTS],
    imports: [...MODULES],
    exports: [...COMPONENTS],
    providers: [...SERVICES]
})
export class CookieModule { }

import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateService } from '@ngx-translate/core';

export class PaginatorLanguage extends MatPaginatorIntl {
  constructor(private readonly translate: TranslateService) {
    super();
    this.getPaginatorIntl();
    this.translate.onLangChange.subscribe(() => {
      this.getPaginatorIntl();
      this.changes.next();
    });
  }

  getPaginatorIntl(): MatPaginatorIntl {
    this.itemsPerPageLabel = this.translate.instant(
      'PAGINATION.ITEMS_PER_PAGE_LABEL'
    );
    this.nextPageLabel = this.translate.instant(
      'PAGINATION.NEXT_PAGE_LABEL'
    );
    this.previousPageLabel = this.translate.instant(
      'PAGINATION.PREVIOUS_PAGE_LABEL'
    );
    this.firstPageLabel = this.translate.instant(
      'PAGINATION.FIRST_PAGE_LABEL'
    );
    this.lastPageLabel = this.translate.instant(
      'PAGINATION.LAST_PAGE_LABEL'
    );
    return this;
  }

  override getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return this.translate.instant('PAGINATION.RANGE_PAGE_LABEL_1', {
        length
      });
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex =
      startIndex < length
        ? Math.min(startIndex + pageSize, length)
        : startIndex + pageSize;
    return this.translate.instant('PAGINATION.RANGE_PAGE_LABEL_2', {
      startIndex: startIndex + 1,
      endIndex,
      length
    });
  }
}

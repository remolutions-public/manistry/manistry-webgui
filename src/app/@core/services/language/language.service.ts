import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../../libraries/models/user.model';
import { environment } from '../../../../environments/environment';

@Injectable()
export class LanguageService {
  private storageNames = {
    lang: 'vault_console_language'
  };

  public supportedLanguages = ['en', 'de'];

  get currentLanguage() {
    return this.translate.currentLang;
  }

  get browserLanguage() {
    let browserLang = navigator['userLanguage'] || navigator['language'];
    if (browserLang == undefined) {
      browserLang = this.translate.getBrowserLang();
    }
    if (!(browserLang == undefined)) {
      browserLang = browserLang.split('-')[0];
      if (this.supportedLanguages.indexOf(browserLang) === -1) {
        browserLang = undefined;
      }
    }
    return this.globalLanguage || browserLang || 'de';
  }

  set globalLanguage(lang: string) {
    localStorage.setItem(this.storageNames.lang, lang);
  }

  get globalLanguage(): string {
    return localStorage.getItem(this.storageNames.lang);
  }

  constructor(
    public readonly translate: TranslateService
  ) {}

  initLanguage() {
    if (!(this.currentLanguage == undefined)) {
      this.translate.use(this.currentLanguage);
    } else if (!(this.globalLanguage == undefined)) {
      this.translate.use(this.globalLanguage);
    } else if (!(this.browserLanguage == undefined)) {
      this.translate.use(this.browserLanguage);
    }
  }

  setLanguage() {
    this.translate.use(this.currentLanguage);
  }

  switchLanguage(code: string) {
    this.translate.use(code);
    this.globalLanguage = code;
  }
}

import { Injectable, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { NotificationService } from '../toast-notification/toast-notification.service';
import { fromEvent, Subscription } from 'rxjs';
import { take, filter } from 'rxjs/operators';
import { TemplatePortal } from '@angular/cdk/portal';
import { debounce } from 'src/app/@core/libraries/basic/debounce.lib';

@Injectable()
export class ContextmenuService {
  public contextmenuClickEvent: Subject<any> = new Subject();
  public contextmenuScrollEvent: Subject<any> = new Subject();

  contextmenuSub?: Subscription;
  contextOverlayRef: OverlayRef | null;

  constructor(
    private readonly http: HttpClient,
    private readonly note: NotificationService,
    public readonly contextOverlay: Overlay
  ) {
    this.init();
  }

  init() {
    this.contextmenuClickEvent.subscribe(() => {
      this.closeContextMenu();
    });
    this.contextmenuScrollEvent.subscribe(() => {
      this.closeContextMenu();
    });
  }

  openContextmenu(
    { x, y }: MouseEvent,
    obj: any,
    contextMenuTemplate: TemplateRef<any>,
    viewContainerRef: ViewContainerRef
  ) {
    debounce('openContextmenu', 10, () => {
      this.contextmenuClickEvent.next(obj);
      const positionStrategy = this.contextOverlay
        .position()
        .flexibleConnectedTo({ x, y })
        .withPositions([
          {
            originX: 'start',
            originY: 'bottom',
            overlayX: 'start',
            overlayY: 'top',
          },
        ]);
      this.contextOverlayRef = this.contextOverlay.create({
        positionStrategy: positionStrategy,
        scrollStrategy: this.contextOverlay.scrollStrategies.close(),
      });
      this.contextOverlayRef.attach(
        new TemplatePortal(contextMenuTemplate, viewContainerRef, {
          $implicit: obj,
        })
      );
      this.contextmenuSub = fromEvent<MouseEvent>(document, 'click')
        .pipe(
          filter((event) => {
            const clickTarget = event.target as HTMLElement;
            return (
              !!this.contextOverlayRef &&
              !this.contextOverlayRef.overlayElement.contains(clickTarget)
            );
          }),
          take(1)
        )
        .subscribe(() => this.closeContextMenu());
    });
  }

  closeContextMenu() {
    this.contextmenuSub && this.contextmenuSub.unsubscribe();
    if (this.contextOverlayRef) {
      this.contextOverlayRef.dispose();
      this.contextOverlayRef = undefined;
    }
  }
}

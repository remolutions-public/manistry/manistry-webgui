import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { getScreenSizeClass } from '../../libraries/basic/env.lib';
import { environment } from '../../../../environments/environment';
import { ThemeService } from '../theme/theme.service';
import { ConfigService } from '../config/config.service';
import { ReCaptchaType } from 'ngx-captcha';

@Injectable()
export class RecaptchaService {
  recaptchaType: 'audio' | 'image' = 'image';

  get recaptchaSiteKey(): string {
    if (this.configService.recaptchaSiteKey == undefined) {
      this.configService.fetchRecaptchaPublicKey();
    }
    return this.configService.recaptchaSiteKey;
  }

  get recaptchaLanguage() {
    return this.translate.currentLang || this.translate.defaultLang;
  }

  get recaptchaTheme() {
    switch (this.themeService.currentTheme.name) {
      case 'dark':
        return 'dark';
      case 'light':
        return 'light';
      default:
        return 'light';
    }
  }

  get recaptchaSize() {
    switch (getScreenSizeClass()) {
      case 'xs':
      case 'is':
        return 'compact';
      default:
        return 'normal';
    }
  }

  constructor(
    private readonly translate: TranslateService,
    private readonly themeService: ThemeService,
    private readonly configService: ConfigService
  ) {}
}

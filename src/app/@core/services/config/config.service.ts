import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { firstValueFrom } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { debounce, hasDebounceRef } from '../../libraries/basic/debounce.lib';

@Injectable()
export class ConfigService {
  settings: any;
  recaptchaSiteKey: string;

  constructor(private readonly http: HttpClient) {}

  async fetchSettings(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.settings == undefined) {
        this.settings = await firstValueFrom(
          this.http.get('/settings', {
            withCredentials: true,
          })
        ).catch((err) => reject(err));
      }
      return resolve(this.settings);
    });
  }

  async fetchRecaptchaPublicKey(): Promise<string> {
    return new Promise(async (resolve, reject) => {
      if (hasDebounceRef('fetchRecaptchaPublicKey')) {
        return resolve(this.recaptchaSiteKey);
      }
      debounce('fetchRecaptchaPublicKey', 250, async () => {
        if (this.recaptchaSiteKey == undefined) {
          const result = (await firstValueFrom(
            this.http.get('/api/v1/auth/recaptcha', {
              withCredentials: true,
            })
          ).catch((err) => reject(err))) as any;
          this.recaptchaSiteKey = result?.recaptchaSiteKey;
        }
        return resolve(this.recaptchaSiteKey);
      });
    });
  }

  async fetchTokenConfig(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (environment.csrfName == undefined) {
        const result = (await firstValueFrom(
          this.http.get('/api/v1/auth/tokenconfig', {
            withCredentials: true,
          })
        ).catch((err) => reject(err))) as any;
        environment.csrfName = result?.csrfTokenName;
      }
      resolve(environment.csrfName);
    });
  }
}

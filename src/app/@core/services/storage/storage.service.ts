import { Injectable } from '@angular/core';
import { HmacSHA256, enc, AES, mode, pad } from 'crypto-js';
import { Subject, Observable, Subscription } from 'rxjs';
import { v4 } from 'uuid';
import { environment } from '../../../../environments/environment';

export declare type BrowserStorageType = 'local' | 'session';
export const BROWSER_STORAGE_TYPES: BrowserStorageType[] = ['local', 'session'];
export interface BrowserStorageOptions {
  default?: BrowserStorageType;
  salt?: string;
  obfuscate?: boolean;
}

// Inspired by https://www.npmjs.com/package/angular-browser-storage
@Injectable()
export class StorageService {
  public static self: StorageService;
  private localStorage: any;
  private sessionStorage: any;
  private storageSubjects: { [key: string]: Subject<any> } = {};
  private storageStates: { [key: string]: Observable<any> } = {};
  private storageSubscriptions: { [key: string]: Subscription } = {};
  private options: BrowserStorageOptions = {
    default: 'local',
    obfuscate: true,
    salt: environment.storageSalt,
  };
  private cryptojs = {
    HmacSHA256,
    enc,
    AES,
    mode,
    pad,
  };

  constructor() {
    this.localStorage = localStorage || window.localStorage;
    this.sessionStorage = sessionStorage || window.sessionStorage;
    for (const storageType of BROWSER_STORAGE_TYPES) {
      for (const key of this.keys(storageType)) {
        this.init(key, storageType);
      }
    }
    if (!StorageService.self) {
      StorageService.self = this;
    }
  }

  public clear(storageType?: BrowserStorageType) {
    const storage = this.getStorage(storageType);
    for (const key in storage) {
      if (storage.hasOwnProperty(key)) {
        this.remove(key, storageType);
      }
    }
    storage.clear();
  }

  public remove(key: string, storageType?: BrowserStorageType) {
    this.init(key, storageType).next(undefined);
    this.getStorage(storageType).removeItem(
      this.options.obfuscate ? this.hash(key) : key
    );
  }

  public trigger(key: string | string[], storageType?: BrowserStorageType) {
    if (!Array.isArray(key)) {
      key = [key];
    }
    for (const k of key) {
      this.init(k, storageType).next(this.get(k, storageType));
    }
  }

  public set(key: string, value: any, storageType?: BrowserStorageType) {
    this.init(key, storageType).next(value);
  }

  public get(key: string, storageType?: BrowserStorageType): any {
    let value = this.getStorage(storageType).getItem(this.hash(key));
    if (!(value == null)) {
      value = this.decrypt(value);
      try {
        value = JSON.parse(value);
      } catch {
        return value;
      }
    }
    return value;
  }

  public getObserver(
    key: string,
    storageType?: BrowserStorageType
  ): Observable<any> {
    this.init(key, storageType);
    return this.storageStates[this.getIndexKey(key, storageType)];
  }

  private getStorageType(storageType?: BrowserStorageType): BrowserStorageType {
    return (
      storageType ? storageType : this.options.default
    ) as BrowserStorageType;
  }

  private getStorage(storageType?: BrowserStorageType): Storage {
    return this.getStorageType(storageType) === 'session'
      ? this.sessionStorage
      : this.localStorage;
  }

  public keys(storageType?: BrowserStorageType): string[] {
    const keys = [];
    const storage = this.getStorage(storageType);
    for (const key in storage) {
      if (storage.hasOwnProperty(key)) {
        keys.push(key);
      }
    }
    return keys;
  }

  private getIndexKey(key: string, storageType?: BrowserStorageType): string {
    return this.getStorageType(storageType) + '_' + key;
  }

  public has(
    key: string,
    storageType?: BrowserStorageType,
    doHash = true
  ): boolean {
    key = doHash ? this.hash(key) : key;
    return !!this.getStorage(storageType).getItem(key);
  }

  private getFingerprint(storageType?: BrowserStorageType): string {
    const key = 'storage_id';
    if (!this.has(key, storageType, false)) {
      this.getStorage(storageType).setItem(key, v4());
    }
    return (
      this.getStorage(storageType)?.getItem(key) || '' + this.options?.salt
    );
  }

  private hash(text: string): string {
    if (!this.options.obfuscate) {
      return text;
    }
    return this.cryptojs
      .HmacSHA256(text, this.getFingerprint())
      .toString(this.cryptojs.enc.Hex);
  }

  private encrypt(text: string): string {
    if (!this.options.obfuscate) {
      return text;
    }
    return this.cryptojs.AES.encrypt(
      text,
      this.getFingerprint(),
      {}
    ).toString();
  }

  private decrypt(text: string): string {
    if (!this.options.obfuscate) {
      return text;
    }
    return this.cryptojs.AES.decrypt(text, this.getFingerprint(), {}).toString(
      this.cryptojs.enc.Utf8
    );
  }

  private init(
    key: string,
    storageType?: BrowserStorageType
  ): Subject<string | undefined> {
    const indexKey = this.getIndexKey(key, storageType);
    if (!this.storageSubjects[indexKey]) {
      this.storageSubjects[indexKey] = new Subject<string>();
      this.storageStates[indexKey] =
        this.storageSubjects[indexKey].asObservable();
      this.storageSubscriptions[indexKey] = this.storageStates[
        indexKey
      ].subscribe((value: any) => {
        if (!(typeof value === 'string')) {
          value = JSON.stringify(value);
        }
        this.getStorage(storageType).setItem(
          this.hash(key),
          this.encrypt(value)
        );
      });
    }
    return this.storageSubjects[indexKey];
  }
}

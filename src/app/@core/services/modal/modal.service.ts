import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { debounce } from '../../libraries/basic/debounce.lib';

@Injectable()
export class ModalService {
  isBackdropActive = false;
  preventClosing = false;

  // triggered by modal.service; listened by backdrop.service
  closeModalSub: Subject<{ class: string; data: any }> = new Subject();

  modalData: any = {};

  onCloseModal(className: string) {
    if (!this.preventClosing) {
      debounce('onCloseModal-' + className, 50, () => {
        this.closeModalSub.next({ class: className, data: this.modalData });
      });
    }
  }
}

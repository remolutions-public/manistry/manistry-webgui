import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';
import { firstValueFrom } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable()
export class RegistryService {
  constructor(
    private readonly http: HttpClient,
    private readonly authService: AuthService
  ) {}

  async fetchCatalog(
    limit: number,
    offset: number,
    filter: string,
    orderBy: string[],
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const paramsObj: any = {};
      if (!(limit == undefined)) {
        paramsObj.limit = String(limit);
      }
      if (!(offset == undefined)) {
        paramsObj.offset = String(offset);
      }
      if (!(orderBy == undefined) && orderBy.length) {
        paramsObj.orderBy = orderBy;
      }
      if (!(filter == undefined) && filter.length) {
        paramsObj.filter = String(filter);
      }
      const params = new HttpParams({
        fromObject: paramsObj,
      });
      const auth = await this.authService.makeAuthHeader();
      // '?' + environment.csrfName + '=' + auth?.token
      firstValueFrom(
        this.http.get('/api/v1/registry', {
          headers: auth?.headers,
          withCredentials: true,
          params,
        })
      )
        .then((result: any[]) => resolve(result))
        .catch((err) => reject(err));
    });
  }

  async fetchTags(
    name: string,
    limit: number,
    offset: number,
    filter: string,
    orderBy: string[],
    onlyCachedTags: boolean,
    force?: boolean
  ): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const paramsObj: any = {};
      if (!(name == undefined)) {
        paramsObj.name = String(name);
      }
      if (!(limit == undefined)) {
        paramsObj.limit = String(limit);
      }
      if (!(offset == undefined)) {
        paramsObj.offset = String(offset);
      }
      if (!(orderBy == undefined) && orderBy.length) {
        paramsObj.orderBy = orderBy;
      }
      if (!(filter == undefined) && filter.length) {
        paramsObj.filter = String(filter);
      }
      if (!(onlyCachedTags == undefined)) {
        paramsObj.cached = onlyCachedTags;
      }
      if (!(force == undefined)) {
        paramsObj.force = force;
      }
      const params = new HttpParams({
        fromObject: paramsObj,
      });
      const auth = await this.authService.makeAuthHeader();
      // '?' + environment.csrfName + '=' + auth?.token
      firstValueFrom(
        this.http.get('/api/v1/registry/tags', {
          headers: auth?.headers,
          withCredentials: true,
          params,
        })
      )
        .then((result: any[]) => resolve(result))
        .catch((err) => reject(err));
    });
  }

  async fetchManifest(name: string, tag: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const paramsObj: any = {};
      if (!(name == undefined)) {
        paramsObj.name = String(name);
      }
      if (!(tag == undefined)) {
        paramsObj.tag = String(tag);
      }
      const params = new HttpParams({
        fromObject: paramsObj,
      });
      const auth = await this.authService.makeAuthHeader();
      // '?' + environment.csrfName + '=' + auth?.token
      firstValueFrom(
        this.http.get('/api/v1/registry/manifests', {
          headers: auth?.headers,
          withCredentials: true,
          params,
        })
      )
        .then((result: any[]) => resolve(result))
        .catch((err) => reject(err));
    });
  }

  async deleteTag(name: string, tag: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const paramsObj: any = {};
      if (!(name == undefined)) {
        paramsObj.name = String(name);
      }
      if (!(tag == undefined)) {
        paramsObj.tag = String(tag);
      }
      const params = new HttpParams({
        fromObject: paramsObj,
      });
      const auth = await this.authService.makeAuthHeader();
      // '?' + environment.csrfName + '=' + auth?.token
      firstValueFrom(
        this.http.delete('/api/v1/registry', {
          headers: auth?.headers,
          withCredentials: true,
          params,
        })
      )
        .then((result: any[]) => resolve(result))
        .catch((err) => reject(err));
    });
  }
}

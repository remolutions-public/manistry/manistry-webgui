import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CookieModule } from './cookie/cookie.module';

import { AuthService } from './auth/auth.service';
import { RoutingService } from './routing/routing.service';
import { NotificationService } from './toast-notification/toast-notification.service';
import { StorageService } from './storage/storage.service';
import { RegistryService } from './registry/registry.service';
import { SidebarService } from './sidebar/sidebar.service';
import { LanguageService } from './language/language.service';
import { ThemeService } from './theme/theme.service';
import { BackdropService } from './backdrop/backdrop.service';
import { ModalService } from './modal/modal.service';
import { MaterialIconsService } from './theme/material-icons/material-icons.service';
import { ContextmenuService } from './contextmenu/contextmenu.service';
import { RecaptchaService } from './recaptcha/recaptcha.service';
import { ConfigService } from './config/config.service';

import { AuthGuard } from '../guards/auth.guard';

import { MatIconModule } from '@angular/material/icon';

const ANGULARMATERIAL = [MatIconModule];

const MODULES = [CommonModule, TranslateModule, CookieModule];

const SERVICES = [
  AuthService,
  RoutingService,
  NotificationService,
  StorageService,
  SidebarService,
  LanguageService,
  ThemeService,
  BackdropService,
  ModalService,
  MaterialIconsService,
  ContextmenuService,
  RegistryService,
  RecaptchaService,
  ConfigService,
];

const ROUTEGUARDS = [AuthGuard];

@NgModule({
  declarations: [],
  imports: [...MODULES, ...ANGULARMATERIAL],
  providers: [...SERVICES, ...ROUTEGUARDS],
})
export class ServicesModule {
  static forRoot(): ModuleWithProviders<ServicesModule> {
    return {
      ngModule: ServicesModule,
      providers: [...SERVICES, ...ROUTEGUARDS],
    };
  }
}

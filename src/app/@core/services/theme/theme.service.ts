import { Injectable, EventEmitter } from '@angular/core';
import { typeIsJSON } from '../../libraries/basic/parser.lib';
import { User } from '../../libraries/models/user.model';
import { environment } from '../../../../environments/environment';

export interface Theme {
  name: string;
  theme: string;
  header: string;
  code: string;
  icon: string;
  hidden?: boolean;
}

@Injectable()
export class ThemeService {
  private storageNames = {
    theme: 'vault_console_theme',
  };

  availableThemes: Theme[] = [
    {
      name: 'dark',
      theme: 'dark',
      header: 'light',
      code: 'THEME.DARK',
      icon: 'moon-outline',
    },
    {
      name: 'white',
      theme: 'white',
      header: 'light',
      code: 'THEME.WHITE',
      icon: 'sunny-outline',
    },
  ];

  activeTheme: Theme = this.globalTheme;

  onThemeChange: EventEmitter<any> = new EventEmitter();

  get currentTheme(): Theme {
    return this.activeTheme;
  }

  set globalTheme(theme: Theme) {
    localStorage.setItem(this.storageNames.theme, JSON.stringify(theme));
  }

  get globalTheme(): Theme {
    const theme = localStorage.getItem(this.storageNames.theme);
    if (!(theme == undefined) && theme.length && typeIsJSON(theme)) {
      return JSON.parse(theme);
    }
    return this.availableThemes[0];
  }

  constructor() {}

  initTheme() {
    this.applyActiveTheme();
  }

  applyActiveTheme() {
    const bodyElement = document.getElementById('main-body');
    if (!(bodyElement == undefined)) {
      bodyElement.setAttribute('class', this.activeTheme.name);
    }
  }

  switchTheme(theme: string) {
    let found = false;
    for (const entry of this.availableThemes) {
      if (entry.name === theme) {
        found = true;
        this.activeTheme = entry;
        this.globalTheme = entry;
        break;
      }
    }
    if (!found) {
      this.activeTheme = this.availableThemes[0];
      this.globalTheme = this.availableThemes[0];
    }
    this.applyActiveTheme();
    this.onThemeChange.emit(this.currentTheme);
  }
}

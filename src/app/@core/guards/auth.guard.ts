import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { RoutingService } from '../services/routing/routing.service';
import { AuthService } from '../services/auth/auth.service';
import { debounce } from '../libraries/basic/debounce.lib';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly routing: RoutingService,
    private readonly authService: AuthService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    return new Promise(async (resolve) => {
      if (await this.authService.isLoggedIn()) {
        resolve(true);
      } else {
        const currentPath = this.routing.getCurrPath();
        this.routing.navigate(['/', 'auth']);
        debounce('auth-guard-set-current-path', 100, () => {
          this.routing.setCurrentPath(currentPath);
        });
        resolve(false);
      }
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ServicesModule } from './services/@services.module';
import { PipesModule } from './pipes/pipes.module';

export const MODULES = [
  CommonModule,
  HttpClientModule,
  ServicesModule,
  PipesModule
];

const DIRECTIVES = [
];

@NgModule({
    imports: [...MODULES],
    declarations: [...DIRECTIVES],
    exports: []
})
export class CoreModule {
}

import { StorageService } from '../../services/storage/storage.service';
import { sleep } from './sleep.lib';
import { environment } from '../../../../environments/environment';

export const hasCsrfToken = async (
  username: string,
  storage?: StorageService,
  breakCounter?: number
): Promise<boolean> => {
  const tokens = await getCsrfToken(username, storage, breakCounter);
  if (tokens == undefined || !tokens.length) {
    return false;
  }
  return true;
};

export const getCsrfToken = async (
  username: string,
  storage?: StorageService,
  breakCounter?: number
): Promise<string> => {
  if (!(breakCounter == undefined)) {
    breakCounter = 0;
  }
  if (breakCounter > 10) {
    return;
  }
  if (!(storage == undefined) || environment.csrfName == undefined) {
    const token = storage.get(environment.csrfName, 'local');
    // check csrf validity
    if (!(username == undefined) && !(token == undefined)) {
      const decoded = JSON.parse(atob(token.split('.')[1]));
      if (decoded.username !== username) {
        return;
      }
    }
    // try even if no username is known
    return token;
  } else {
    await sleep(10);
    return await getCsrfToken(username, StorageService.self, breakCounter++);
  }
};

export const setCsrfToken = async (
  token: string,
  username: string,
  storage?: StorageService,
  breakCounter?: number
): Promise<boolean> => {
  if (!breakCounter) {
    breakCounter = 0;
  }
  if (breakCounter > 10) {
    return false;
  }
  if (!(storage == undefined) || environment.csrfName == undefined) {
    if (!(token == undefined)) {
      // is csrf for the requesting user?
      if (!(username == undefined)) {
        const decoded = JSON.parse(atob(token.split('.')[1]));
        if (decoded.username !== username) {
          return false;
        }
      }
      storage.set(environment.csrfName, token, 'local');
      return true;
    } else {
      storage.remove(environment.csrfName, 'local');
    }
  } else {
    await sleep(10);
    return await setCsrfToken(
      token,
      username,
      StorageService.self,
      breakCounter++
    );
  }
};

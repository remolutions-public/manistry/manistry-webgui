export const parseBoolean = (input: any): boolean => {
  if (
    !(input == undefined) &&
    (input === 1 || input === 'true' || input === true)
  ) {
    return true;
  } else {
    return false;
  }
};

export const typeIsBoolean = (input: any): boolean => {
  if (
    input === 'true' ||
    input === 'false' ||
    input === true ||
    input === false
  ) {
    return true;
  }
  return false;
};

export const typeIsNumeric = (input: any): boolean => {
  if (input == undefined || Number.isNaN(input)) {
    return false;
  }
  try {
    const parsedInput = parseFloat(input);
    if (Number.isNaN(parsedInput)) {
      return false;
    }
    if (typeof parsedInput === 'number') {
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
};

export const typeIsArray = (input: any): boolean => {
  if (Array.isArray(input)) {
    return true;
  }
  return false;
};

export const typeIsObject = (input: any): boolean => {
  if (typeof input === 'object') {
    return true;
  }
  return false;
};

export const typeIsString = (input: any): boolean => {
  if (!typeIsBoolean(input) && !typeIsNumeric(input)) {
    if (typeof input === 'string') {
      return true;
    }
  }
  return false;
};

export const typeIsJSON = (input: any): boolean => {
  if (
    !(input == undefined) &&
    typeIsString(input) &&
    (input.charAt(0) === '[' || input.charAt(0) === '{')
  ) {
    return true;
  }
  return false;
};

export const isWithinAbsoluteMaxLength = (input: any): boolean => {
  if (String(input).length > 1024) {
    return false;
  }
  return true;
};

export const isBase64 = (input: string): boolean => {
  const base64regex = new RegExp(
    /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/
  );
  if (base64regex.test(input)) {
    return true;
  }
  return false;
};

export const base64Decode = (str: string) => {
  if (isBase64(str)) {
    return window.atob(str);
  }
  return str;
};

export const base64Encode = (str: string) => {
  if (!isBase64(str)) {
    return window.btoa(str);
  }
  return str;
};

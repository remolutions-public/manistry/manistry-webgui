export const encodeFileToBase64 = async (file: File): Promise<string> => {
  let binary = '';
  const bytes = new Uint8Array(await file.arrayBuffer());
  for (const byte of bytes) {
    binary += String.fromCharCode(byte);
  }
  return window.btoa(binary);
};

export const getScreenSizeClass = (): string => {
  const width = window.innerWidth;
  if (width < 270) {
    return 'xs';
  }
  if (width < 400) {
    return 'is';
  }
  if (width < 576) {
    return 'sm';
  }
  if (width < 768) {
    return 'md';
  }
  if (width < 992) {
    return 'lg';
  }
  if (width < 1200) {
    return 'xl';
  }
  if (width < 1400) {
    return 'xxl';
  }
  if (width < 2000) {
    return 'xxxl';
  }
  return 'xxxxl';
};

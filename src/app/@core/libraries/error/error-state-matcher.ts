import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

export class ErrorKeys {
  public static objKeys(input: any) {
    // Make Object.keys() available to DOM
    if (input && input !== 'undefined' && typeof input === 'object') {
      return Object.keys(input);
    }
    return null;
  }

  public static firstKey(input: any) {
    const keys = ErrorKeys.objKeys(input);
    if (keys) {
      return keys[0];
    }
    return null;
  }

  public static firstObject(input: any) {
    const keys = ErrorKeys.objKeys(input);
    if (keys) {
      return input[keys[0]];
    }
    return null;
  }

  public static firstError(form: any, attr: string) {
    const key = ErrorKeys.firstKey(form.get(attr).errors);
    return {
      key,
      val: form.get(attr).errors[key]
    };
  }

  public static errMsgKeys(errVal: any) {
    if (errVal) {
      return {
        requiredLength: errVal.requiredLength,
        diffLength: errVal.actualLength - errVal.requiredLength,
        chars: errVal.chars
      };
    }
  }
}

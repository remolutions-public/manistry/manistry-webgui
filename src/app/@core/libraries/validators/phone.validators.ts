import {
  FormControl,
} from '@angular/forms';

export class PhoneValidators {
  phone(input: FormControl) {
    const PHONE_REGEXP = /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$/g;
    const regex = new RegExp(PHONE_REGEXP);
    if (regex.test(input.value) || input.value === '') {
      return null;
    }
    return {
      phoneinvalid: true,
    };
  }
}

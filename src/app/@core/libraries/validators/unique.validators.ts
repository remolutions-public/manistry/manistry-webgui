import {
  AsyncValidatorFn,
  ValidationErrors,
  AbstractControl
} from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { User } from '../models/user.model';

export class UniqueValidators {
  usernameUnique(
    userService: UserService,
    delay: number,
    currentUser?: User
  ): AsyncValidatorFn {
    let timeOutRef: any;
    return async (ctrl: AbstractControl): Promise<ValidationErrors> => {
      return new Promise((resolve) => {
        if (timeOutRef) {
          window.clearTimeout(timeOutRef);
        }
        timeOutRef = setTimeout(() => {
          if (
            ctrl.value !== '' &&
            ctrl.dirty &&
            ctrl.value !== userService.user?.username
          ) {
            // is it the same user email as before?
            if (
              currentUser &&
              currentUser.username &&
              currentUser.username.toLowerCase() === ctrl.value.toLowerCase()
            ) {
              return resolve(null);
            }
            userService
              .checkUsername(ctrl.value)
              .then((res) => {
                if (res.result) {
                  resolve(null);
                } else {
                  resolve({ usernamenotunique: true });
                }
              })
              .catch(() => {
                resolve(null);
              });
          } else {
            resolve(null);
          }
        }, delay);
      });
    };
  }

  emailUnique(
    userService: UserService,
    delay: number,
    currentUser?: User
  ): AsyncValidatorFn {
    let timeOutRef: any;
    return async (ctrl: AbstractControl): Promise<ValidationErrors> => {
      return new Promise((resolve) => {
        if (timeOutRef) {
          window.clearTimeout(timeOutRef);
        }
        timeOutRef = setTimeout(() => {
          if (
            ctrl.value !== '' &&
            ctrl.dirty &&
            ctrl.value !== userService.user?.email
          ) {
            // is it the same user email as before?
            if (
              currentUser &&
              currentUser.email &&
              currentUser.email.toLowerCase() === ctrl.value.toLowerCase()
            ) {
              return resolve(null);
            }
            userService
              .checkEmail(ctrl.value)
              .then((res) => {
                if (res.result) {
                  resolve(null);
                } else {
                  resolve({ emailnotunique: true });
                }
              })
              .catch(() => {
                resolve(null);
              });
          } else {
            resolve(null);
          }
        }, delay);
      });
    };
  }
}

import { FormControl } from '@angular/forms';

export class CharValidators {
  invalidChars(input: FormControl) {
    if (!input.value.length) {
      return null;
    }

    const matchPatterns = new RegExp("[$&§+,:;=?@#|'<>.^*()%!_`´/]");
    if (matchPatterns.test(input.value)) {
      return { containsInvalidChars: { chars: '[a-z]|[-]' } };
    }

    return null;
  }
}

import { FormControl } from '@angular/forms';

export class NumberValidators {
  isNumeric(input: FormControl) {
    if (!(input.value == undefined)) {
      try {
        if (!Number.isNaN(parseFloat(input.value))) {
          return null;
        }
      } catch (e) {}
    }
    return {
      isNotNumeric: true,
    };
  }
}

import {
  FormControl,
} from '@angular/forms';

export class PasswordValidators {
  password(input: FormControl) {
    // // min 6 chars
    // // at least 1 number and 1 char
    // const regex = new RegExp(/^(?=.*\d)(?=.*[a-zA-Z]).{6,}$/);
    // if (regex.test(input.value) || input.value === '') {
    //   return null;
    // }
    if (input.value && input.value.length > 7) {
      return null;
    }
    return {
      pwinvalid: true,
    };
  }
}

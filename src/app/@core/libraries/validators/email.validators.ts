import {
  FormControl,
} from '@angular/forms';

export class EmailValidators {
  email(input: FormControl) {
    const part1 = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*/;
    const part2 = /@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    const regex = new RegExp(part1.source + part2.source);
    if (regex.test(input.value) || input.value === '') {
      return null;
    }
    return {
      emailinvalid: true,
    };
  }
}

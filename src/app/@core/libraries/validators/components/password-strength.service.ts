import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class PasswordStrengthService {
  // is triggered by register.component; listened by password-strength.component
  public readonly componentViewChanged: Subject<boolean> = new Subject();
}

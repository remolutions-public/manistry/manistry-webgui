import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { PasswordStrengthComponent } from './password-strength.component';
import { PasswordStrengthService } from './password-strength.service';

export const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
];

export const COMPONENTS = [
  PasswordStrengthComponent,
];

export const SERVICES = [
  PasswordStrengthService,
];

@NgModule({
    imports: [...MODULES],
    declarations: [...COMPONENTS],
    exports: [...COMPONENTS],
    providers: [...SERVICES]
})
export class PasswordStrengthModule {}

import { Component, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { PasswordStrengthService } from './password-strength.service';
import { Subscription } from 'rxjs';

export class StrengthClass {
  key: string;
  threshold: number;
  translation: string;
}

@Component({
  selector: 'app-password-strength',
  templateUrl: './password-strength.component.html',
  styleUrls: ['./password-strength.component.scss']
})
export class PasswordStrengthComponent implements AfterViewInit, OnDestroy {
  @Input() username: string;
  @Input() password: string;

  score = 0;

  strengthClasses: StrengthClass[] = [
    {
      key: 'weak-password',
      threshold: 10,
      translation: 'AUTH.PWSTRENGTH.WEAK'
    },
    {
      key: 'okay-password',
      threshold: 35,
      translation: 'AUTH.PWSTRENGTH.OKAY'
    },
    {
      key: 'good-password',
      threshold: 80,
      translation: 'AUTH.PWSTRENGTH.GOOD'
    },
    {
      key: 'strong-password',
      threshold: 100,
      translation: 'AUTH.PWSTRENGTH.STRONG'
    }
  ];

  refreshTimerRef: any;

  private subs: Subscription[] = [];

  constructor(private readonly pwStrengthService: PasswordStrengthService) {}

  ngAfterViewInit() {
    this.registerTimerRefs();
    this.subs.push(
      this.pwStrengthService.componentViewChanged.subscribe((val) => {
        if (val) {
          this.registerTimerRefs();
        } else {
          this.clearTimerRefs();
        }
      })
    );
  }

  ionViewDidEnter() {
    this.registerTimerRefs();
  }

  registerTimerRefs() {
    if (!this.refreshTimerRef) {
      this.refreshTimerRef = setInterval(() => {
        this.updateScore();
      }, 100);
    }
  }

  get strength(): StrengthClass {
    for (const row of this.strengthClasses) {
      if (row.threshold >= this.score) {
        return row;
      }
    }
  }

  updateScore() {
    this.score = 0;
    // password < 4
    if (
      !this.password ||
      this.password.length < 4 ||
      (this.username &&
        (this.password.toLowerCase() === this.username.toLowerCase() ||
          this.password.toLowerCase().indexOf(this.username.toLowerCase()) >
            -1))
    ) {
      return;
    }
    // password length
    this.score += this.password.length * 1;
    this.score += this.repetitionCheck(4, this.password).length * 1;
    // password has 3 numbers
    if (this.password.match(/(.*[0-9].*[0-9].*[0-9])/)) {
      this.score += 5;
    }
    // password has 2 symbols
    if (
      this.password.match(
        /(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/
      )
    ) {
      this.score += 5;
    }
    // password has Upper and Lower chars
    if (this.password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
      this.score += 8;
    }
    // password has number and chars
    if (this.password.match(/([a-zA-Z])/) && this.password.match(/([0-9])/)) {
      this.score += 13;
    }
    // password has number and symbol
    if (
      this.password.match(/([!,@,#,$,%,^,&,*,?,_,~])/) &&
      this.password.match(/([0-9])/)
    ) {
      this.score += 13;
    }
    // password has char and symbol
    if (
      this.password.match(/([!,@,#,$,%,^,&,*,?,_,~])/) &&
      this.password.match(/([a-zA-Z])/)
    ) {
      this.score += 13;
    }
    // password is just a numbers or chars
    if (this.password.match(/^\w+$/) || this.password.match(/^\d+$/)) {
      this.score -= 10;
    }

    // verifing 0 < score < 100
    if (this.score < 0) {
      this.score = 0;
    }
    if (this.score > 100) {
      this.score = 100;
    }
  }

  repetitionCheck(pLen: number, str: string) {
    let res = '';
    let repeated = false;
    for (let i = 0; i < str.length; i++) {
      let j: number;
      for (j = 0; j < pLen && j + i + pLen < str.length; j++) {
        repeated = repeated && str.charAt(j + i) === str.charAt(j + i + pLen);
      }
      if (j < pLen) {
        repeated = false;
      }
      if (repeated) {
        i += pLen - 1;
        repeated = false;
      } else {
        res += str.charAt(i);
      }
    }
    return res;
  }

  ionViewDidLeave() {
    this.clearTimerRefs();
  }

  ngOnDestroy() {
    this.clearTimerRefs();
    if (this.subs) {
      this.subs.forEach((sub) => sub.unsubscribe());
    }
  }

  clearTimerRefs() {
    if (this.refreshTimerRef) {
      clearInterval(this.refreshTimerRef);
      this.refreshTimerRef = undefined;
    }
  }
}

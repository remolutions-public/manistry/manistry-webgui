export class Session {
  username?: string;
  email?: string;
  isadmin?: boolean;
  tokenExpiration?: number;
}

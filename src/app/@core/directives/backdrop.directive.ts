import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appBackdropModalSlot]',
})
export class BackdropDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

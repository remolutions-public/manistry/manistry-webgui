FROM node:18 as builder

COPY . ./

ENV BUILD_DIR=/app/src/app/

COPY . ${BUILD_DIR}

WORKDIR ${BUILD_DIR}

RUN npm install
RUN npm run build:prod


# https://github.com/bitnami/bitnami-docker-nginx/tree/master/1.22/debian-11
FROM bitnami/nginx:1.22

USER 0

ENV BUILD_DIR=/app/src/app/

RUN rm -rf /opt/bitnami/nginx/conf/nginx.conf
COPY ./configs/nginx.conf /opt/bitnami/nginx/conf/nginx.conf
COPY ./scripts/entrypoint.sh /opt/bitnami/scripts/nginx/custom-entrypoint.sh
COPY --from=builder --chown=nobody ${BUILD_DIR}/www /app

USER 1001

CMD ["sh", "/opt/bitnami/scripts/nginx/custom-entrypoint.sh"]

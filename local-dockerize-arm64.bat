set IMAGE_VERSION=v1.0.30
set IMAGE_ARCH=arm64
set IMAGE_PATH="registry.gitlab.com/remolutions-public/manistry/manistry-webgui/manistry-webgui"


FOR /F "" %%g IN ('docker ps -a -q --filter="name=%IMAGE_PATH%"') do (SET CONTAINER_CHECK_RESULT=%%g)
if [%CONTAINER_CHECK_RESULT%]==[] (
  echo no container found, skipping...
) else (
  echo removing container %CONTAINER_CHECK_RESULT%
  docker rm -f %CONTAINER_CHECK_RESULT%
)


@REM you might want to change your docker builder
@REM check existing builders: 
@REM docker buildx create --name armbuilder
@REM docker buildx use armbuilder
@REM docker buildx inspect --bootstrap

@REM Only when you want to change to an existing builder..
@REM docker buildx create --use
@REM docker buildx ls
@REM docker context use desktop-linux


Powershell.exe -executionpolicy remotesigned "npm install"
Powershell.exe -executionpolicy remotesigned "npm run build:prod"


docker buildx build --platform linux/arm64 -t %IMAGE_PATH%:%IMAGE_VERSION%-%IMAGE_ARCH% -f "Dockerfile_nginx" --push .


pause

const PROXY_CONFIG = {
  "/api": {
    target: "http://localhost:3030",
    secure: false,
    changeOrigin: true,
    timeout: 15000,
    bypass: function (req, res, proxyOptions) {
      switch (req.url) {
        case "/healthcheck":
          res.end(JSON.stringify({ message: "success" }));
          return true;
        case "/settings":
          res.end(
            JSON.stringify({
              legalNoticeLink: "http://localhost:4200",
              privacyPolicyLink: "http://localhost:4200",
            })
          );
          return true;
      }
      return undefined;
    },
  },
};

module.exports = PROXY_CONFIG;
